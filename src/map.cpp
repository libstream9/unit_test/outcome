#include <stream9/outcome/map.hpp>

#include "error.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

namespace st9 = stream9;

BOOST_AUTO_TEST_SUITE(map_)

    template<typename> struct type_of;

    BOOST_AUTO_TEST_SUITE(same_error_type_)

        BOOST_AUTO_TEST_CASE(no_error_)
        {
            using O1 = st9::outcome<int, errc1>;
            using O2 = st9::outcome<std::string, errc1>;

            auto f1 = [](int v) -> O1 { return 2 * v; };
            auto f2 = [](int v) -> std::string { return std::to_string(v); };

            auto o = f1(10) | f2;

            static_assert(std::same_as<decltype(o), O2>);
            BOOST_REQUIRE(o.has_value());
            BOOST_TEST(o.value() == "20");
        }

        BOOST_AUTO_TEST_CASE(error_on_first_func_)
        {
            using O1 = st9::outcome<int, errc1>;
            using O2 = st9::outcome<std::string, errc1>;

            auto f1 = [](int v) -> O1 { (void)v; return errc1::x; };
            auto f2 = [](int v) -> std::string { return std::to_string(v); };

            auto o = f1(30) | f2;

            static_assert(std::same_as<decltype(o), O2>);
            BOOST_REQUIRE(o.has_error());
            BOOST_CHECK(o.error() == errc1::x);
        }

        BOOST_AUTO_TEST_CASE(error_on_second_func_)
        {
            using O1 = st9::outcome<int, errc1>;
            using O2 = st9::outcome<std::string, errc1>;

            auto f1 = [](int v) -> O1 { return 2 * v; };
            auto f2 = [](int v) -> O2 { (void)v; return errc1::x; };

            auto o = f1(30) | f2;

            static_assert(std::same_as<decltype(o), O2>);
            BOOST_REQUIRE(o.has_error());
            BOOST_CHECK(o.error() == errc1::x);
        }

    BOOST_AUTO_TEST_SUITE_END() // same_error_type_

    BOOST_AUTO_TEST_SUITE(different_error_type_)

        BOOST_AUTO_TEST_CASE(no_error_)
        {
            using O1 = st9::outcome<int, errc1>;
            using O2 = st9::outcome<std::string, errc2>;
            using O3 = st9::outcome<std::string, std::error_code>;

            auto f1 = [](int v) -> O1 { return 2 * v; };
            auto f2 = [](int v) -> O2 { return std::to_string(v); };

            auto o = f1(50) | f2;

            static_assert(std::same_as<decltype(o), O3>);
            BOOST_REQUIRE(o.has_value());
            BOOST_TEST(o.value() == "100");
        }

    BOOST_AUTO_TEST_SUITE_END() // different_error_type_

BOOST_AUTO_TEST_SUITE_END() // map_

} // namespace testing
