#include <stream9/outcome.hpp>

#include "error.hpp"

#include <string>
#include <type_traits>

#include <boost/test/unit_test.hpp>

#include <stream9/errors.hpp>

namespace testing {

namespace st9 = stream9;

BOOST_AUTO_TEST_SUITE(outcome_)

BOOST_AUTO_TEST_SUITE(regular_)

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        using O = st9::outcome<int, errc1>;
        static_assert(std::is_nothrow_default_constructible_v<O>);

        O o;

        BOOST_TEST(o.has_value());
        BOOST_TEST(o.value() == 0);
    }

    BOOST_AUTO_TEST_CASE(non_trivial_destructor_)
    {
        using O = st9::outcome<std::string, errc1>;
        static_assert(std::is_nothrow_default_constructible_v<O>);

        std::string s;
        s.resize(256);
        O o { s };

        BOOST_TEST(o.has_value());
        BOOST_TEST(o.value() == s);
        BOOST_TEST(o->size() == 256);
    }

    BOOST_AUTO_TEST_CASE(value_copy_constructor_)
    {
        using O = st9::outcome<std::string, errc1>;
        static_assert(std::is_constructible_v<O, std::string const&>);

        std::string s = "foo";
        O o { s };

        BOOST_TEST(o.has_value());
        BOOST_TEST(o.value() == s);
    }

    BOOST_AUTO_TEST_CASE(value_move_constructor_)
    {
        using O = st9::outcome<std::string, errc1>;
        static_assert(std::is_nothrow_constructible_v<O, std::string>);

        std::string s = "foo";
        O o { std::move(s) };

        BOOST_TEST(o.has_value());
        BOOST_TEST(o.value() == "foo");
    }

    BOOST_AUTO_TEST_CASE(value_copy_assignment_)
    {
        using O = st9::outcome<std::string, errc1>;
        static_assert(std::is_assignable_v<O, std::string const&>);

        {
            std::string s = "foo";
            O o;

            o = s;

            BOOST_TEST(o.has_value());
            BOOST_TEST(o.value() == s);
        }

        {
            std::string s = "foo";
            O o { errc1::x };

            o = s;

            BOOST_REQUIRE(o.has_value());
            BOOST_TEST(o.value() == s);
        }
    }

    BOOST_AUTO_TEST_CASE(value_move_assignment_)
    {
        using O = st9::outcome<std::string, errc1>;
        static_assert(std::is_assignable_v<O, std::string>);

        {
            std::string s = "foo";
            O o;

            o = std::move(s);

            BOOST_TEST(o.has_value());
            BOOST_TEST(o.value() == "foo");
        }

        {
            std::string s = "foo";
            O o { errc1::x };

            o = std::move(s);

            BOOST_REQUIRE(o.has_value());
            BOOST_TEST(o.value() == "foo");
        }
    }

    BOOST_AUTO_TEST_CASE(error_copy_constructor_)
    {
        using O = st9::outcome<std::string, errc1>;
        static_assert(std::is_nothrow_constructible_v<O, errc1 const&>);

        auto e = errc1::x;
        O o { e };

        BOOST_REQUIRE(!o.has_value());
        BOOST_CHECK(o.error() == e);
    }

    BOOST_AUTO_TEST_CASE(error_move_constructor_)
    {
        using O = st9::outcome<std::string, errc1>;
        static_assert(std::is_nothrow_constructible_v<O, errc1>);

        auto e = errc1::x;
        O o { std::move(e) };

        BOOST_REQUIRE(!o.has_value());
        BOOST_CHECK(o.error() == errc1::x);
    }

    BOOST_AUTO_TEST_CASE(error_copy_assignment_)
    {
        using O = st9::outcome<std::string, errc1>;
        static_assert(std::is_nothrow_assignable_v<O, errc1>);

        {
            O o { errc1::x };
            auto e1 = errc1::y;

            o = e1;

            BOOST_REQUIRE(!o.has_value());
            BOOST_CHECK(o.error() == e1);
        }

        {
            O o { "foo" };
            auto e1 = errc1::x;

            o = e1;

            BOOST_REQUIRE(!o.has_value());
            BOOST_CHECK(o.error() == e1);
        }
    }

    BOOST_AUTO_TEST_CASE(error_move_assignment_)
    {
        using O = st9::outcome<std::string, errc1>;
        static_assert(std::is_nothrow_assignable_v<O, errc1>);

        {
            O o { errc1::x };
            auto e1 = errc1::y;

            o = std::move(e1);

            BOOST_REQUIRE(!o.has_value());
            BOOST_CHECK(o.error() == errc1::y);
        }

        {
            O o { "foo" };
            auto e1 = errc1::x;

            o = std::move(e1);

            BOOST_REQUIRE(!o.has_value());
            BOOST_CHECK(o.error() == errc1::x);
        }
    }

    BOOST_AUTO_TEST_CASE(explicit_constructor_)
    {
        using O = st9::outcome<int, long>;
        static_assert(std::is_nothrow_constructible_v<O, st9::value_tag, int>);
        static_assert(std::is_nothrow_constructible_v<O, st9::error_tag, long>);

        {
            O o { st9::value_tag(), 100 };

            BOOST_TEST(o.has_value());
            BOOST_TEST(o.value() == 100);
        }
        {
            O o { st9::error_tag(), 100 };

            BOOST_TEST(!o.has_value());
            BOOST_TEST(o.error() == 100);
        }
    }

    BOOST_AUTO_TEST_CASE(dual_constructor_)
    {
        using O = st9::outcome<int, long>;
        static_assert(std::is_nothrow_constructible_v<O, int, long>);

        {
            O o { 100, 200 };

            BOOST_TEST(o.has_value());
            BOOST_TEST(o.value() == 100);
            BOOST_TEST(o.has_error());
            BOOST_TEST(o.error() == 200);
        }
    }

    BOOST_AUTO_TEST_CASE(copy_constructor_1_)
    {
        using O = st9::outcome<int, errc1>;

        O o1 { 1 };
        O o2 { o1 };

        BOOST_TEST(o1.value() == o2.value());
    }

    BOOST_AUTO_TEST_CASE(copy_constructor_2_)
    {
        using O = st9::outcome<int, errc1>;

        O o1 { errc1::x };
        O o2 { o1 };

        BOOST_CHECK(o1.error() == o2.error());
        BOOST_CHECK(o2.error() == errc1::x);
    }

    BOOST_AUTO_TEST_CASE(move_constructor_1_)
    {
        using O = st9::outcome<std::string, errc1>;

        O o1 { "foo" };
        O o2 { std::move(o1) };

        BOOST_TEST(o2.value() == "foo");
    }

    BOOST_AUTO_TEST_CASE(move_constructor_2_)
    {
        using O = st9::outcome<int, std::string>;

        O o1 { "foo" };
        O o2 { std::move(o1) };

        BOOST_TEST(!o2.has_value());
        BOOST_TEST(o2.error() == "foo");
    }

    BOOST_AUTO_TEST_CASE(value_1_)
    {
        using O = st9::outcome<int, errc1>;

        O o1 { 100 };

        using T1 = decltype(o1.value());
        static_assert(std::same_as<T1, int&>);

        using T2 = decltype(static_cast<O const&>(o1).value());
        static_assert(std::same_as<T2, int const&>);

        using T3 = decltype(static_cast<O&&>(o1).value());
        static_assert(std::same_as<T3, int&&>);
    }

    BOOST_AUTO_TEST_CASE(value_2_)
    {
        using O = st9::outcome<int, errc1>;

        O o1 { 100 };

        using T1 = decltype(*o1);
        static_assert(std::same_as<T1, int&>);

        using T2 = decltype(*static_cast<O const&>(o1));
        static_assert(std::same_as<T2, int const&>);

        using T3 = decltype(*static_cast<O&&>(o1));
        static_assert(std::same_as<T3, int&&>);

        using T4 = decltype(*static_cast<O const&&>(o1));
        static_assert(std::same_as<T4, int const&&>);
    }

    BOOST_AUTO_TEST_CASE(error_1_)
    {
        using O = st9::outcome<int, errc1>;

        O o1 { errc1::x };
        O const o2 { errc1::x };

        static_assert(std::same_as<decltype(o1.error()), errc1&>);
        static_assert(std::same_as<decltype(o2.error()), errc1 const&>);
        static_assert(std::same_as<decltype(static_cast<O&&>(o1).error()), errc1&&>);
        static_assert(std::same_as<decltype(static_cast<O const&&>(o2).error()), errc1 const&&>);
    }

    BOOST_AUTO_TEST_CASE(or_throw_1_)
    {
        using O = st9::outcome<int, errc1>;

        O o1 { errc1::x };
        O const o2 { errc1::x };

        static_assert(std::same_as<decltype(o1.or_throw()), int&>);
        static_assert(std::same_as<decltype(o2.or_throw()), int const&>);
        static_assert(std::same_as<decltype(static_cast<O&&>(o1).or_throw()), int&&>);
        static_assert(std::same_as<decltype(static_cast<O const&&>(o2).or_throw()), int const&&>);

        auto fn = []() -> O { return errc1::x; };

        static_assert(std::same_as<decltype(fn().or_throw()), int&&>);
        BOOST_CHECK_THROW(fn().or_throw(), st9::error);
    }

    BOOST_AUTO_TEST_CASE(arrow_1_)
    {
        using O = st9::outcome<std::string, errc1>;

        O o1 { "foo" };

        BOOST_TEST(o1->size() == 3);

        O const o2 { "12345" };

        BOOST_TEST(o2->size() == 5);
    }

    BOOST_AUTO_TEST_CASE(equality_1_)
    {
        using O = st9::outcome<int, errc1>;

        O o1 { 100 };
        O o2 { o1 };
        O o3 { 200 };
        O o4 { errc1::x };
        O o5 { o4 };

        BOOST_CHECK(o1 == o2);
        BOOST_CHECK(o1 != o3);
        BOOST_CHECK(o4 == o5);
        BOOST_CHECK(o4 != o1);
    }

    BOOST_AUTO_TEST_CASE(equality_2_)
    {
        using O1 = st9::outcome<int, errc1>;
        using O2 = st9::outcome<long, errc1>;

        O1 o1 { 100 };
        O1 o2 { o1 };
        O2 o3 { 200 };

        BOOST_CHECK(o1 == o2);
        BOOST_CHECK(o1 != o3);
    }

    BOOST_AUTO_TEST_CASE(equality_with_error_1_)
    {
        using O1 = st9::outcome<int, errc1>;

        O1 o1 { 100 };
        O1 o2 { errc1::x };

        BOOST_CHECK(o1 != errc1::x);
        BOOST_CHECK(o2 == errc1::x);
    }

    BOOST_AUTO_TEST_CASE(equality_with_error_2_)
    {
        using O1 = st9::outcome<int, std::error_code>;

        O1 o1 { 100 };
        O1 o2 { errc1::x };

        BOOST_CHECK(o1 != errc1::x);
        BOOST_CHECK(o2 == errc1::x);
    }

    BOOST_AUTO_TEST_CASE(partial_ordering_)
    {
        using O = st9::outcome<int, errc1>;

        O o1 { 1 };
        O o2 { 2 };
        O o3 { errc1::x };

        BOOST_CHECK(o1 < o2);
        BOOST_CHECK(o2 > o1);
        BOOST_CHECK(!(o1 < o3));
        BOOST_CHECK(!(o1 > o1));
    }

BOOST_AUTO_TEST_SUITE_END() // regular_

BOOST_AUTO_TEST_SUITE(trivial_object_)

    struct X {
        int m_v;

        X() = default;
        X(int a) noexcept : m_v { a } {}
        X(X const&) = default;
        X& operator=(X const&) = default;
        X(X&&) = default;
        X& operator=(X&&) = default;
        ~X() = default;
    };

    BOOST_AUTO_TEST_CASE(type_traits_)
    {
        using O = st9::outcome<X, errc1>;

        static_assert(std::is_trivial_v<X>);

        static_assert(std::is_default_constructible_v<O>);
        static_assert(std::is_copy_constructible_v<O>);
        static_assert(std::is_copy_assignable_v<O>);
        static_assert(std::is_move_constructible_v<O>);
        static_assert(std::is_move_assignable_v<O>);

        static_assert(std::is_constructible_v<O, int>);
        static_assert(std::is_constructible_v<O, int const>);
        static_assert(std::is_constructible_v<O, int&>);
        static_assert(std::is_constructible_v<O, int const&>);
        static_assert(std::is_constructible_v<O, errc1>);
        static_assert(std::is_constructible_v<O, errc1 const>);
        static_assert(std::is_constructible_v<O, errc1&>);
        static_assert(std::is_constructible_v<O, errc1 const&>);
    }

BOOST_AUTO_TEST_SUITE_END() // trivial_object

BOOST_AUTO_TEST_SUITE(non_default_constructible_object_)

    struct X {
        int a;

        X(int a) : a { a } {}

        X(X const&) = default;
        X& operator=(X const&) = default;
        X(X&&) = default;
        X& operator=(X&&) = default;
    };

    BOOST_AUTO_TEST_CASE(type_traits_)
    {
        using O = st9::outcome<X, errc1>;

        static_assert(!std::is_default_constructible_v<X>);
        static_assert(std::is_copy_constructible_v<X>);
        static_assert(std::is_copy_assignable_v<X>);
        static_assert(std::is_move_constructible_v<X>);
        static_assert(std::is_move_assignable_v<X>);

        static_assert(!std::is_default_constructible_v<O>);
        static_assert(std::is_copy_constructible_v<O>);
        static_assert(std::is_copy_assignable_v<O>);
        static_assert(std::is_move_constructible_v<O>);
        static_assert(std::is_move_assignable_v<O>);

        static_assert(std::is_constructible_v<O, int>);
        static_assert(std::is_constructible_v<O, int const>);
        static_assert(std::is_constructible_v<O, int&>);
        static_assert(std::is_constructible_v<O, int const&>);
        static_assert(std::is_constructible_v<O, errc1>);
        static_assert(std::is_constructible_v<O, errc1 const>);
        static_assert(std::is_constructible_v<O, errc1&>);
        static_assert(std::is_constructible_v<O, errc1 const&>);
    }

BOOST_AUTO_TEST_SUITE_END() // non_default_constructible_object_

BOOST_AUTO_TEST_SUITE(non_copyable_object_)

    struct X {
        int a;

        X() = default;
        X(int a) : a { a } {}

        X(X const&) = delete;
        X& operator=(X const&) = delete;
        X(X&&) = default;
        X& operator=(X&&) = default;
    };

    BOOST_AUTO_TEST_CASE(type_traits_)
    {
        using O = st9::outcome<X, errc1>;

        static_assert(std::is_default_constructible_v<X>);
        static_assert(!std::is_copy_constructible_v<X>);
        static_assert(!std::is_copy_assignable_v<X>);
        static_assert(std::is_move_constructible_v<X>);
        static_assert(std::is_move_assignable_v<X>);

        static_assert(std::is_default_constructible_v<O>);
        static_assert(!std::is_copy_constructible_v<O>);
        static_assert(!std::is_copy_assignable_v<O>);
        static_assert(std::is_move_constructible_v<O>);
        static_assert(std::is_move_assignable_v<O>);

        static_assert(std::is_constructible_v<O, int>);
        static_assert(std::is_constructible_v<O, int const>);
        static_assert(std::is_constructible_v<O, int&>);
        static_assert(std::is_constructible_v<O, int const&>);
        static_assert(std::is_constructible_v<O, errc1>);
        static_assert(std::is_constructible_v<O, errc1 const>);
        static_assert(std::is_constructible_v<O, errc1&>);
        static_assert(std::is_constructible_v<O, errc1 const&>);
    }

BOOST_AUTO_TEST_SUITE_END() // non_copyable_object_

BOOST_AUTO_TEST_SUITE(non_movable_object_)

    struct X {
        int a;

        X() = default;
        X(int a) : a { a } {}

        X(X const&) = delete;
        X& operator=(X const&) = delete;
        X(X&&) = delete;
        X& operator=(X&&) = delete;
    };

    BOOST_AUTO_TEST_CASE(type_traits_)
    {
        using O = st9::outcome<X, errc1>;

        static_assert(std::is_default_constructible_v<X>);
        static_assert(!std::is_copy_constructible_v<X>);
        static_assert(!std::is_copy_assignable_v<X>);
        static_assert(!std::is_move_constructible_v<X>);
        static_assert(!std::is_move_assignable_v<X>);

        static_assert(std::is_default_constructible_v<O>);
        static_assert(!std::is_copy_constructible_v<O>);
        static_assert(!std::is_copy_assignable_v<O>);
        static_assert(!std::is_move_constructible_v<O>);
        static_assert(!std::is_move_assignable_v<O>);

        static_assert(std::is_constructible_v<O, int>);
        static_assert(std::is_constructible_v<O, int const>);
        static_assert(std::is_constructible_v<O, int&>);
        static_assert(std::is_constructible_v<O, int const&>);
        static_assert(std::is_constructible_v<O, errc1>);
        static_assert(std::is_constructible_v<O, errc1 const>);
        static_assert(std::is_constructible_v<O, errc1&>);
        static_assert(std::is_constructible_v<O, errc1 const&>);
    }

BOOST_AUTO_TEST_SUITE_END() // non_movable_object_

BOOST_AUTO_TEST_SUITE(const_lvalue_reference_)

    BOOST_AUTO_TEST_CASE(constructors_)
    {
        using O1 = st9::outcome<int const&, errc1>;

        static_assert(!std::default_initializable<O1>);

        // with value
        static_assert(!std::constructible_from<O1, int>);
        static_assert(!std::constructible_from<O1, int const>);
        static_assert(std::constructible_from<O1, int&>);
        static_assert(std::constructible_from<O1, int const&>);
        static_assert(!std::constructible_from<O1, int&&>);
        static_assert(!std::constructible_from<O1, int const&&>);

        // with error
        static_assert(std::constructible_from<O1, errc1>);
        static_assert(std::constructible_from<O1, errc1 const>);
        static_assert(std::constructible_from<O1, errc1&>);
        static_assert(std::constructible_from<O1, errc1 const&>);
        static_assert(std::constructible_from<O1, errc1&&>);
        static_assert(std::constructible_from<O1, errc1 const&&>);

        // with compatible outcome (1)
        using O2 = st9::outcome<int&, errc1>;

        static_assert(std::constructible_from<O1, O2>);
        static_assert(std::constructible_from<O1, O2 const>);
        static_assert(std::constructible_from<O1, O2&>);
        static_assert(std::constructible_from<O1, O2 const&>);
        static_assert(std::constructible_from<O1, O2&&>);
        static_assert(std::constructible_from<O1, O2 const&&>);

        // with compatible outcome (2)
        using O3 = st9::outcome<int const&, std::error_code>;

        static_assert(std::constructible_from<O3, O1>);
        static_assert(std::constructible_from<O3, O1 const>);
        static_assert(std::constructible_from<O3, O1&>);
        static_assert(std::constructible_from<O3, O1 const&>);
        static_assert(std::constructible_from<O3, O1&&>);
        static_assert(std::constructible_from<O3, O1 const&&>);
    }

    BOOST_AUTO_TEST_CASE(assignments_)
    {
        using O1 = st9::outcome<int const&, errc1>;

        // with value
        static_assert(!std::is_assignable_v<O1, int>);
        static_assert(!std::is_assignable_v<O1, int const>);
        static_assert(std::is_assignable_v<O1, int&>);
        static_assert(std::is_assignable_v<O1, int const&>);
        static_assert(!std::is_assignable_v<O1, int&&>);
        static_assert(!std::is_assignable_v<O1, int const&&>);

        // with error
        static_assert(std::is_assignable_v<O1, errc1>);
        static_assert(std::is_assignable_v<O1, errc1 const>);
        static_assert(std::is_assignable_v<O1, errc1&>);
        static_assert(std::is_assignable_v<O1, errc1 const&>);
        static_assert(std::is_assignable_v<O1, errc1&&>);
        static_assert(std::is_assignable_v<O1, errc1 const&&>);

        // with compatible outcome (1)
        using O2 = st9::outcome<int&, errc1>;

        static_assert(std::is_assignable_v<O1, O2>);
        static_assert(std::is_assignable_v<O1, O2 const>);
        static_assert(std::is_assignable_v<O1, O2&>);
        static_assert(std::is_assignable_v<O1, O2 const&>);
        static_assert(std::is_assignable_v<O1, O2&&>);
        static_assert(std::is_assignable_v<O1, O2 const&&>);

        // with compatible outcome (2)
        using O3 = st9::outcome<int const&, std::error_code>;

        static_assert(std::is_assignable_v<O3, O1>);
        static_assert(std::is_assignable_v<O3, O1 const>);
        static_assert(std::is_assignable_v<O3, O1&>);
        static_assert(std::is_assignable_v<O3, O1 const&>);
        static_assert(std::is_assignable_v<O3, O1&&>);
        static_assert(std::is_assignable_v<O3, O1 const&&>);
    }

    BOOST_AUTO_TEST_CASE(constructor_1_)
    {
        using O1 = st9::outcome<int const&, errc1>;

        int i = 100;
        O1 o1 { i };

        BOOST_TEST(o1.has_value());
        BOOST_TEST(o1.value() == 100);

        O1 o2 { errc1::x };

        BOOST_TEST(!o2.has_value());
        BOOST_CHECK(o2.error() == errc1::x);
    }

    BOOST_AUTO_TEST_CASE(constructor_2_)
    {
        using O = st9::outcome<int&, errc1>;

        int i = 100;
        O o { i };

        BOOST_TEST(o.has_value());
        BOOST_TEST(o.value() == 100);
    }

    BOOST_AUTO_TEST_CASE(value_1_)
    {
        using O = st9::outcome<int const&, errc1>;

        int i = 100;
        O o1 { i };

        using T1 = decltype(o1.value());
        static_assert(std::same_as<T1, int const&>);

        using T2 = decltype(std::move(o1).value());
        static_assert(std::same_as<T2, int const&>);

        O const o2 { i };

        using T3 = decltype(o2.value());
        static_assert(std::same_as<T3, int const&>);

        using T4 = decltype(std::move(o2).value());
        static_assert(std::same_as<T4, int const&>);
    }

    BOOST_AUTO_TEST_CASE(value_2_)
    {
        using O = st9::outcome<int&, errc1>;

        int i = 100;
        O o1 { i };

        using T1 = decltype(o1.value());
        static_assert(std::same_as<T1, int&>);

        using T2 = decltype(std::move(o1).value());
        static_assert(std::same_as<T2, int&>);

        O const o2 { i };

        using T1 = decltype(o1.value());
        static_assert(std::same_as<T1, int&>);

        using T2 = decltype(std::move(o1).value());
        static_assert(std::same_as<T2, int&>);
    }

    BOOST_AUTO_TEST_CASE(arrow_1_)
    {
        using O = st9::outcome<std::string const&, errc1>;

        std::string s = "foo";
        O o1 { s };

        BOOST_TEST(o1->size() == 3);
    }

    BOOST_AUTO_TEST_CASE(arrow_2_)
    {
        using O = st9::outcome<std::string&, errc1>;

        std::string s = "foo";
        O o1 { s };

        o1->append("bar");

        BOOST_TEST(s == "foobar");
    }

BOOST_AUTO_TEST_SUITE_END() // const_lvalue_reference_

BOOST_AUTO_TEST_SUITE(lvalue_reference_)

    BOOST_AUTO_TEST_CASE(constructors_)
    {
        using O1 = st9::outcome<int&, errc1>;

        static_assert(!std::default_initializable<O1>);

        // with value
        static_assert(!std::constructible_from<O1, int>);
        static_assert(!std::constructible_from<O1, int const>);
        static_assert(std::constructible_from<O1, int&>);
        static_assert(!std::constructible_from<O1, int const&>);
        static_assert(!std::constructible_from<O1, int&&>);
        static_assert(!std::constructible_from<O1, int const&&>);

        // with compatible outcome
        using O2 = st9::outcome<int&, std::error_code>;

        static_assert(std::constructible_from<O2, O1>);
        static_assert(std::constructible_from<O2, O1 const>);
        static_assert(std::constructible_from<O2, O1&>);
        static_assert(std::constructible_from<O2, O1 const&>);
        static_assert(std::constructible_from<O2, O1&&>);
        static_assert(std::constructible_from<O2, O1 const&&>);

        // with incompatible outcome
        using O3 = st9::outcome<int const&, errc1>;

        static_assert(!std::constructible_from<O1, O3>);
        static_assert(!std::constructible_from<O1, O3 const>);
        static_assert(!std::constructible_from<O1, O3&>);
        static_assert(!std::constructible_from<O1, O3 const&>);
        static_assert(!std::constructible_from<O1, O3&&>);
        static_assert(!std::constructible_from<O1, O3 const&&>);

        // with both value and error
        static_assert(!std::constructible_from<O1, int, errc1>);
        static_assert(!std::constructible_from<O1, int const, errc1>);
        static_assert(std::constructible_from<O1, int&, errc1>);
        static_assert(!std::constructible_from<O1, int const&, errc1>);
        static_assert(!std::constructible_from<O1, int&&, errc1>);
        static_assert(!std::constructible_from<O1, int const&&, errc1>);
    }

    BOOST_AUTO_TEST_CASE(dual_construction_)
    {
        using O1 = st9::outcome<int&, errc1>;

        int i = 100;
        auto e = errc1::x;

        O1 v { i, e };

        BOOST_TEST(v.has_value());
        BOOST_TEST(v.value() == i);

        BOOST_TEST(v.has_error());
        BOOST_TEST(v.error() == e);
    }

    BOOST_AUTO_TEST_CASE(assignments_)
    {
        using O1 = st9::outcome<int&, errc1>;

        // with value
        static_assert(!std::is_assignable_v<O1, int>);
        static_assert(!std::is_assignable_v<O1, int const>);
        static_assert(std::is_assignable_v<O1, int&>);
        static_assert(!std::is_assignable_v<O1, int const&>);
        static_assert(!std::is_assignable_v<O1, int&&>);
        static_assert(!std::is_assignable_v<O1, int const&&>);

        // with compatible outcome
        using O2 = st9::outcome<int&, std::error_code>;

        static_assert(std::is_assignable_v<O2, O1>);
        static_assert(std::is_assignable_v<O2, O1 const>);
        static_assert(std::is_assignable_v<O2, O1&>);
        static_assert(std::is_assignable_v<O2, O1 const&>);
        static_assert(std::is_assignable_v<O2, O1&&>);
        static_assert(std::is_assignable_v<O2, O1 const&&>);

        // with incompatible outcome
        using O3 = st9::outcome<int const&, errc1>;

        static_assert(!std::is_assignable_v<O1, O3>);
        static_assert(!std::is_assignable_v<O1, O3 const>);
        static_assert(!std::is_assignable_v<O1, O3&>);
        static_assert(!std::is_assignable_v<O1, O3 const&>);
        static_assert(!std::is_assignable_v<O1, O3&&>);
        static_assert(!std::is_assignable_v<O1, O3 const&&>);
    }

BOOST_AUTO_TEST_SUITE_END() // lvalue_reference_

BOOST_AUTO_TEST_SUITE(rvalue_reference_)

    BOOST_AUTO_TEST_CASE(constructors_)
    {
        using O1 = st9::outcome<int&&, errc1>;

        static_assert(!std::default_initializable<O1>);

        // with value
        static_assert(std::constructible_from<O1, int>);
        static_assert(!std::constructible_from<O1, int const>);
        static_assert(!std::constructible_from<O1, int&>);
        static_assert(!std::constructible_from<O1, int const&>);
        static_assert(std::constructible_from<O1, int&&>);
        static_assert(!std::constructible_from<O1, int const&&>);

        // with compatible outcome
        using O2 = st9::outcome<int&&, std::error_code>;

        static_assert(std::constructible_from<O2, O1>);
        static_assert(std::constructible_from<O2, O1 const>);
        static_assert(std::constructible_from<O2, O1&>);
        static_assert(std::constructible_from<O2, O1 const&>);
        static_assert(std::constructible_from<O2, O1&&>);
        static_assert(std::constructible_from<O2, O1 const&&>);

        // with incompatible outcome
        using O3 = st9::outcome<int const&, errc1>;

        static_assert(!std::constructible_from<O1, O3>);
        static_assert(!std::constructible_from<O1, O3 const>);
        static_assert(!std::constructible_from<O1, O3&>);
        static_assert(!std::constructible_from<O1, O3 const&>);
        static_assert(!std::constructible_from<O1, O3&&>);
        static_assert(!std::constructible_from<O1, O3 const&&>);
    }

    BOOST_AUTO_TEST_CASE(assignments_)
    {
        using O1 = st9::outcome<int&&, errc1>;

        // with value
        static_assert(std::is_assignable_v<O1, int>);
        static_assert(!std::is_assignable_v<O1, int const>);
        static_assert(!std::is_assignable_v<O1, int&>);
        static_assert(!std::is_assignable_v<O1, int const&>);
        static_assert(std::is_assignable_v<O1, int&&>);
        static_assert(!std::is_assignable_v<O1, int const&&>);

        // with compatible outcome
        using O2 = st9::outcome<int&&, std::error_code>;

        static_assert(std::is_assignable_v<O2, O1>);
        static_assert(std::is_assignable_v<O2, O1 const>);
        static_assert(std::is_assignable_v<O2, O1&>);
        static_assert(std::is_assignable_v<O2, O1 const&>);
        static_assert(std::is_assignable_v<O2, O1&&>);
        static_assert(std::is_assignable_v<O2, O1 const&&>);

        // with incompatible outcome
        using O3 = st9::outcome<int const&, errc1>;

        static_assert(!std::is_assignable_v<O1, O3>);
        static_assert(!std::is_assignable_v<O1, O3 const>);
        static_assert(!std::is_assignable_v<O1, O3&>);
        static_assert(!std::is_assignable_v<O1, O3 const&>);
        static_assert(!std::is_assignable_v<O1, O3&&>);
        static_assert(!std::is_assignable_v<O1, O3 const&&>);
    }

BOOST_AUTO_TEST_SUITE_END() // rvalue_reference_

BOOST_AUTO_TEST_SUITE_END() // outcome_

} // namespace testing
