#include <stream9/outcome/type_traits.hpp>

#include "error.hpp"

#include <string>
#include <type_traits>

#include <boost/test/unit_test.hpp>

#include <stream9/errors.hpp>

namespace testing {

namespace st9 = stream9;

template<typename> struct type_of;

BOOST_AUTO_TEST_SUITE(common_error_type_)

BOOST_AUTO_TEST_CASE(same_type_)
{
    using E1 = st9::common_error_type_t<errc1, errc1>;
    using E2 = st9::common_error_type_t<int, int>;
    using E3 = st9::common_error_type_t<std::error_code, std::error_code>;
    using E4 = st9::common_error_type_t<std::string, std::string>;

    static_assert(std::same_as<E1, errc1>);
    static_assert(std::same_as<E2, int>);
    static_assert(std::same_as<E3, std::error_code>);
    static_assert(std::same_as<E4, std::string>);
}

BOOST_AUTO_TEST_CASE(error_code_enum_)
{
    using E1 = st9::common_error_type_t<errc1, errc2>;
    using E2 = st9::common_error_type_t<errc1, std::error_code>;
    using E3 = st9::common_error_type_t<std::error_code, errc2>;

    static_assert(std::same_as<E1, std::error_code>);
    static_assert(std::same_as<E2, std::error_code>);
    static_assert(std::same_as<E3, std::error_code>);
}

BOOST_AUTO_TEST_CASE(have_common_type_)
{
    using E1 = st9::common_error_type_t<int, long>;

    static_assert(std::same_as<E1, long>);
}

BOOST_AUTO_TEST_SUITE_END() // common_error_type_

BOOST_AUTO_TEST_SUITE(common_type_)

    BOOST_AUTO_TEST_CASE(different_value_type_)
    {
        using O1 = st9::outcome<int, errc1>;
        using O2 = st9::outcome<long, errc1>;

        //type_of<std::common_type_t<O1, O2>>::is;
        static_assert(std::same_as<std::common_type_t<O1, long>, O2>);
        static_assert(std::same_as<std::common_type_t<O2, int>, O2>);
        static_assert(std::same_as<std::common_type_t<O1, O2>, O2>);
    }

    BOOST_AUTO_TEST_CASE(different_error_type_)
    {
        using O1 = st9::outcome<int, errc1>;
        using O2 = st9::outcome<int, errc2>;
        using O3 = st9::outcome<int, std::error_code>;

        //type_of<std::common_type_t<O1, O2>>::is;
        static_assert(std::same_as<std::common_type_t<O1, errc1>, O1>);
        static_assert(std::same_as<std::common_type_t<O1, errc2>, O3>);
        static_assert(std::same_as<std::common_type_t<O1, std::error_code>, O3>);
        static_assert(std::same_as<std::common_type_t<O1, O2>, O3>);
        static_assert(std::same_as<std::common_type_t<O1, O3>, O3>);
    }

BOOST_AUTO_TEST_SUITE_END() // common_type_

} // namespace testing
