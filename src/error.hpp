#ifndef STREAM9_CORE_TEST_OUTCOME_SRC_ERROR_HPP
#define STREAM9_CORE_TEST_OUTCOME_SRC_ERROR_HPP

#include <system_error>

namespace testing {

enum class errc1 {
    ok = 0,
    x, y, z,
};

inline std::error_category const&
error_category1()
{
    static struct impl : std::error_category {
        char const* name() const noexcept override { return "errc1"; }
        std::string message(int e) const override
        {
            switch (static_cast<errc1>(e)) {
                using enum errc1;
                case ok: return "ok";
                case x: return "x";
                case y: return "y";
                case z: return "z";
            }
            return "unknown";
        }
    } instance;

    return instance;
}

inline std::error_code
make_error_code(errc1 e)
{
    return { static_cast<int>(e), error_category1() };
}

enum class errc2 {
    ok = 0,
    x, y, z,
};

inline std::error_category const&
error_category2()
{
    static struct impl : std::error_category {
        char const* name() const noexcept override { return "errc2"; }
        std::string message(int e) const override
        {
            switch (static_cast<errc2>(e)) {
                using enum errc2;
                case ok: return "ok";
                case x: return "x";
                case y: return "y";
                case z: return "z";
            }
            return "unknown";
        }
    } instance;

    return instance;
}

inline std::error_code
make_error_code(errc2 e)
{
    return { static_cast<int>(e), error_category2() };
}

} // namespace testing

namespace std {

template<>
struct is_error_code_enum<testing::errc1>
    : true_type {};

template<>
struct is_error_code_enum<testing::errc2>
    : true_type {};

} // namespace std

#endif // STREAM9_CORE_TEST_OUTCOME_SRC_ERROR_HPP
